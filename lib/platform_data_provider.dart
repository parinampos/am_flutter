import 'dart:async';

import 'package:flutter/services.dart';

class PlatformDataProvider <T> {

  static const _platform = const MethodChannel('com.am_flutter/data');

  Future<T> getPlatformData(String method) async {
    T result;
    try {
      result = await _platform.invokeMethod(method);
    } on PlatformException catch (e) {
      return Future.error(e);
    }
    return Future.value(result);
  }
}
