import 'package:am_flutter/style/AmposColors.dart';
import 'package:flutter/material.dart';

class DashboardTextStyle {
  static final messageTextStyle = TextStyle(
    color: AmposColors.white,
    fontSize: 10,
    fontStyle: FontStyle.italic,
    fontWeight: FontWeight.bold,
  );

  static final detailTextStyle = TextStyle(
    color: AmposColors.prussianBlue,
    fontSize: 14,
    fontWeight: FontWeight.bold,
  );

  static final detailNumberTextStyle = TextStyle(
    color: AmposColors.deepCarrotOrange,
    fontSize: 40,
    fontWeight: FontWeight.bold,
  );

  static final subDetailTextStyle = TextStyle(
    color: AmposColors.prussianBlue,
    fontSize: 12,
    fontWeight: FontWeight.bold,
  );

  static final subDetailNumberTextStyle = TextStyle(
    color: AmposColors.deepCarrotOrange,
    fontSize: 16,
    fontWeight: FontWeight.bold,
  );
}
