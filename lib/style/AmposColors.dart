import 'dart:ui';

class AmposColors {
  static final transparent = Color(0x00ffffff);
  static final white = Color(0xffffffff);
  static final prussianBlue = Color(0xff0b3051);
  static final turquoise = Color(0xff38c4c2);
  static final deepCarrotOrange = Color(0xfff36f31);
  
  static final title = Color(0x16000000);
}