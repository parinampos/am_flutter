import 'package:am_flutter/dashboard/DragDropItem.dart';
import 'package:am_flutter/style/textstyle/DashboardTextStyle.dart';
import 'package:flutter/material.dart';

class DragDropItemHeaderWidget extends StatelessWidget {
  DragDropItem item;

  DragDropItemHeaderWidget(DragDropItem item) {
    this.item = item;
  }

  @override
  Widget build(BuildContext context) {
    return buildTitle(item);
  }

  Widget buildTitle(DragDropItem item) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(8),
      color: item.titleBackgroundColor,
      child: Text(
        item.title,
        style: DashboardTextStyle.detailTextStyle,
      ),
    );
  }
}
