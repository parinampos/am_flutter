import 'package:am_flutter/dashboard/DragDropItem.dart';
import 'package:am_flutter/dashboard/dragdrop_item_widget.dart';
import 'package:am_flutter/domain/entity/dashboard.dart';
import 'package:flutter/material.dart';

class DragDropWidget extends StatelessWidget {
  final Future<Dashboard> dashboard;
  var items = [];

  DragDropWidget({Key key, @required this.dashboard});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Dashboard>(
      future: dashboard,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          debugPrint("TOW: snapshot.hasData");
          makeItems(snapshot.data);
          //    return buildGrid();
          return buildSilverGrid();
        }
        debugPrint("TOW: snapshot.has NO Data");
        return SliverPadding(
          padding: EdgeInsets.all(8),
        );
      },
    );
  }

  List makeItems(Dashboard item) {
    items = [
      DragDropItem(item, WidgetFeature.REWARD, WidgetType.MENU),
      DragDropItem(item, WidgetFeature.LEARNING, WidgetType.MENU),
      DragDropItem(item, WidgetFeature.ANNOUNCEMENT, WidgetType.MENU),
      DragDropItem(item, WidgetFeature.MESSAGING, WidgetType.MENU),
      DragDropItem(item, WidgetFeature.COACHING, WidgetType.MENU),
      DragDropItem(item, WidgetFeature.ASSIGNMENT, WidgetType.MENU),
      DragDropItem(item, WidgetFeature.SURVEY, WidgetType.MENU),
      DragDropItem(item, WidgetFeature.KPI, WidgetType.MENU),
      DragDropItem(item, WidgetFeature.SEND_STAR, WidgetType.ACTION),
      DragDropItem(item, WidgetFeature.NEW_ANNOUNCEMENT, WidgetType.ACTION),
      DragDropItem(item, WidgetFeature.NEW_MESSAGING, WidgetType.ACTION),
      DragDropItem(item, WidgetFeature.CLASSROOM_TRAINING, WidgetType.MENU),
      DragDropItem(item, WidgetFeature.ESS, WidgetType.MENU),
    ];
    return items;
  }

  Widget buildSilverGrid() {
    return SliverGrid(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200.0,
//        mainAxisSpacing: 8.0,
//        crossAxisSpacing: 8.0,
        childAspectRatio: 8.0 / 8.0,
      ),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return Container(
            child: DragDropItemWidget(items[index]),
          );
        },
        childCount: items.length,
      ),
    );
  }

  Widget buildGrid() {
    return GridView.count(
      shrinkWrap: true,
      crossAxisCount: 2,
      padding: EdgeInsets.all(16.0),
      childAspectRatio: 8.0 / 9.0,
      children: items.map((item) {
        return DragDropItemWidget(item);
      }).toList(),
    );
  }
}
