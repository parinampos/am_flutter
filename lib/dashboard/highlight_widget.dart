import 'package:am_flutter/domain/entity/dashboard.dart';
import 'package:flutter/material.dart';

class HighlightWidget extends StatelessWidget {

  final Future<Dashboard> dashboard;

  HighlightWidget({Key key, @required this.dashboard});

  @override
  Widget build(BuildContext context) {
    Widget redDotIcon = new Container(
      margin: const EdgeInsets.all(8),
      width: 10.0,
      height: 10.0,
      decoration: new BoxDecoration(
        color: Colors.redAccent,
        shape: BoxShape.circle,
      ),
    );

    Widget _buildThumbnail(String imagePath) {
      return Container(
        color: Colors.grey[100],
        padding: EdgeInsets.only(top: 16, bottom: 16),
        alignment: Alignment.center,
        child: Image.network(
          imagePath,
          width: 70,
        ),
      );
    }

    Widget _buildText(String moduleName, String title) {
      return Container(
        margin: EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              moduleName.toUpperCase(),
              style: TextStyle(
                  fontSize: 12.0,
                  fontWeight: FontWeight.w100,
                  color: Color(0xFF0B3051)
              ),
            ),
            Text(
              title,
              style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.w600,
                  color: Color(0xFF0B3051)
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
          ],
        ),
      );
    }

    Stack _buildWidgetItem(String imagePath, String moduleName, String title, bool isUnread) {
      Stack stack = Stack(
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _buildThumbnail(imagePath),
                _buildText(moduleName, title)
              ],
            )
          ]
      );

      if (isUnread) {
        stack.children.add(
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                redDotIcon
              ],
            )
        );
      }
      return stack;
    }

    Widget _buildHighlightsWithData(List<DHighlight> highlights) {
      return SizedBox(
        height: 200,
        child: ListView.builder(
          shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: highlights.length,
            itemBuilder: (_, index) {
              DHighlight item = highlights[index];
              return Container(
                  width: 150,
                  child: Card(
                      child: _buildWidgetItem(item.imageUrl, item.feature, item.title, item.hasNewNotification)
                  )
              );
            }
        ),
      );
    }

    Widget _buildHighlights() {
      return FutureBuilder<Dashboard>(
        future: dashboard,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return _buildHighlightsWithData(snapshot.data.highlights);
          } else {
            return Container();
          }
        },
      );
    }

    return _buildHighlights();
  }
}