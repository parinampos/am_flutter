import 'dart:ui';

import 'package:am_flutter/domain/entity/dashboard.dart';
import 'package:am_flutter/style/AmposColors.dart';

class DragDropItem {
  WidgetFeature feature;
  WidgetType type;
  String title;
  Color titleBackgroundColor;
  String imagePath;
  String message;
  List<String> detail;

  DragDropItem(Dashboard item, WidgetFeature feature, WidgetType type) {
    this.feature = feature;
    this.type = type;
    this.title = getTitle(feature);
    this.titleBackgroundColor = getTitleBackgroundColor(type);
    this.imagePath = getImagePath(feature);
    this.message = getMessage(feature);
    this.detail = makeDetail(feature, item);
  }

  String getTitle(WidgetFeature feature) {
    switch (feature) {
      case WidgetFeature.ANNOUNCEMENT:
        return "ANNOUNCEMENT";
      case WidgetFeature.ASSIGNMENT:
        return "ASSIGNMENT";
      case WidgetFeature.CLASSROOM_TRAINING:
        return "CLASSROOM TRAINING";
      case WidgetFeature.COACHING:
        return "1:1 COACHING";
      case WidgetFeature.ESS:
        return "ESS";
      case WidgetFeature.KPI:
        return "KPIS";
      case WidgetFeature.LEARNING:
        return "LEARNING";
      case WidgetFeature.MESSAGING:
        return "MESSAGING";
      case WidgetFeature.MY_TEAM:
        return "MY TEAM";
      case WidgetFeature.NEW_ANNOUNCEMENT:
        return "NEW ANNOUNCEMENT";
      case WidgetFeature.NEW_MESSAGING:
        return "NEW MESSAGING";
      case WidgetFeature.REWARD:
        return "REWARD";
      case WidgetFeature.SEND_STAR:
        return "SEND STAR";
      case WidgetFeature.SURVEY:
        return "SURVEY";
      default:
        return "";
    }
  }

  Color getTitleBackgroundColor(WidgetType type) {
    switch (type) {
      case WidgetType.MENU:
        return AmposColors.title;
      case WidgetType.ACTION:
        return AmposColors.turquoise;
      default:
        return AmposColors.title;
    }
  }

  String getImagePath(WidgetFeature feature) {
    switch (feature) {
      case WidgetFeature.ANNOUNCEMENT:
      case WidgetFeature.NEW_ANNOUNCEMENT:
        return "images/dashboard_item/icon_announcement.png";
      case WidgetFeature.ASSIGNMENT:
        return "images/dashboard_item/icon_assignment.png";
      case WidgetFeature.CLASSROOM_TRAINING:
        return "images/dashboard_item/icon_classroom.png";
      case WidgetFeature.COACHING:
        return "images/dashboard_item/icon_oneonone.png";
      case WidgetFeature.ESS:
        return "images/dashboard_item/icon_ess.png";
      case WidgetFeature.KPI:
        return "images/dashboard_item/icon_kpi.png";
      case WidgetFeature.LEARNING:
        return "images/dashboard_item/icon_learning_1.png";
      case WidgetFeature.MESSAGING:
        return "images/dashboard_item/icon_messaging.png";
      case WidgetFeature.NEW_MESSAGING:
        return "images/dashboard_item/icon_messaging_1.png";
      case WidgetFeature.REWARD:
        return "images/dashboard_item/icon_gift.png";
      case WidgetFeature.SEND_STAR:
        return "images/dashboard_item/icon_send_star.png";
      case WidgetFeature.SURVEY:
        return "images/dashboard_item/icon_survey.png";
      default:
        return "";
    }
  }

  String getMessage(WidgetFeature feature) {
    switch (feature) {
      case WidgetFeature.NEW_ANNOUNCEMENT:
        return "Create!";
      case WidgetFeature.NEW_MESSAGING:
        return "Start!";
      case WidgetFeature.SEND_STAR:
        return "Click\nMe!";
      default:
        return "";
    }
  }

  List<String> makeDetail(WidgetFeature feature, Dashboard item) {
    if (item == null) return [];
    switch (feature) {
      case WidgetFeature.ANNOUNCEMENT:
        return [
          item.announcement.newAnnouncement.toString(),
          "New\nAnnouncement"
        ];
      case WidgetFeature.ASSIGNMENT:
        return [item.assignment.newAssignment.toString(), "New\nTasks"];
      case WidgetFeature.CLASSROOM_TRAINING:
        return [item.classroomTraining.today.toString(), "Class\nToday"];
      case WidgetFeature.COACHING:
        return [
          item.coaching.today.toString(),
          "Session Today",
          item.coaching.missed.toString(),
          "Session Missed"
        ];
      case WidgetFeature.ESS:
        return ["Employee\nSelf Service"];
      case WidgetFeature.KPI:
        return ["Key Performance\nIndicator Monitoring"];
      case WidgetFeature.LEARNING:
        return [
          item.learning.assigned.toString(),
          "Assigned",
          item.learning.open.toString(),
          "Open",
          item.learning.inProgress.toString(),
          "In Progresss"
        ];
      case WidgetFeature.MESSAGING:
        return [item.messaging.newMessage.toString(), "New\nMessage"];
      case WidgetFeature.NEW_ANNOUNCEMENT:
        return ["Create\nNew Announcement"];
      case WidgetFeature.NEW_MESSAGING:
        return ["Start\nNew Conversation"];
      case WidgetFeature.REWARD:
        return [item.reward.receivedStar.toString(), "Stars You\nHave Got"];
      case WidgetFeature.SEND_STAR:
        return [item.sendStar.remainingQuota.toString(), "Stars\nTo Send"];
      case WidgetFeature.SURVEY:
        return ["New\nSurveys"];
      default:
        return [];
    }
  }
}

enum WidgetFeature {
  ANNOUNCEMENT,
  ASSIGNMENT,
  CLASSROOM_TRAINING,
  COACHING,
  ESS,
  KPI,
  LEARNING,
  MESSAGING,
  MY_TEAM,
  NEW_ANNOUNCEMENT,
  NEW_MESSAGING,
  REWARD,
  SEND_STAR,
  SURVEY
}

enum WidgetType { ACTION, MENU }
