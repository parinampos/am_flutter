import 'package:am_flutter/dashboard/DragDropItem.dart';
import 'package:am_flutter/style/textstyle/DashboardTextStyle.dart';
import 'package:flutter/material.dart';

class DragDropItemDetailWidget extends StatelessWidget {
  DragDropItem item;

  DragDropItemDetailWidget(DragDropItem item) {
    this.item = item;
  }

  @override
  Widget build(BuildContext context) {
    return buildDetail(item.feature);
  }

  Widget buildDetail(WidgetFeature feature) {
    switch (feature) {
      case WidgetFeature.ANNOUNCEMENT:
      case WidgetFeature.ASSIGNMENT:
      case WidgetFeature.CLASSROOM_TRAINING:
      case WidgetFeature.MESSAGING:
      case WidgetFeature.REWARD:
      case WidgetFeature.SEND_STAR:
        {
          return buildDetailContentWithNumber();
        }
      case WidgetFeature.COACHING:
      case WidgetFeature.LEARNING:
        {
          return buildMultipleContent();
        }
      case WidgetFeature.ESS:
      case WidgetFeature.KPI:
      case WidgetFeature.NEW_ANNOUNCEMENT:
      case WidgetFeature.NEW_MESSAGING:
      case WidgetFeature.SURVEY:
      default:
        {
          return buildDetailContent();
        }
    }
  }

  Widget buildDetailContent() {
    return Container(
      padding: EdgeInsets.all(8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            item.detail[0],
            style: DashboardTextStyle.detailTextStyle,
          )
        ],
      ),
    );
  }

  Widget buildDetailContentWithNumber() {
    return Container(
      padding: EdgeInsets.all(8),
      child: Row(
        children: [
          Text(
            item.detail[0],
            style: DashboardTextStyle.detailNumberTextStyle,
          ),
          SizedBox(
            width: 8,
          ),
          Text(
            item.detail[1],
            style: DashboardTextStyle.detailTextStyle,
          ),
        ],
      ),
    );
  }

  Widget buildMultipleContent() {
    return Container(
      height: 64,
      padding: EdgeInsets.all(8),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: buildSubDetail(item.detail)),
    );
  }

  List<Widget> buildSubDetail(List detail) {
    return makeData(detail, 4).map((i) {
      return Expanded(child: buildSubColumn(i));
    }).toList();
  }

  Widget buildSubColumn(List detail) {
    var subDetailItems = makeData(detail, 2).map((i) {
      return buildSubDetailRow(i);
    }).toList();

    return Column(
        mainAxisAlignment: MainAxisAlignment.end, children: subDetailItems);
  }

  Widget buildSubDetailRow(List detail) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.baseline,
        textBaseline: TextBaseline.alphabetic,
        children: [
          Container(
            child: Text(
              detail[0],
              textAlign: TextAlign.center,
              style: DashboardTextStyle.subDetailNumberTextStyle,
            ),
          ),
          SizedBox(
            width: 8,
          ),
          Text(
            detail[1],
            overflow: TextOverflow.ellipsis,
            style: DashboardTextStyle.subDetailTextStyle,
          )
        ],
      ),
    );
  }

  List<List<String>> makeData(List detail, int dividerCount) {
    return detail.fold<List<List<String>>>([[]], (container, element) {
      if (container.last.length == dividerCount) {
        container.add([]);
      }
      container.last.add(element);
      return container;
    });
  }
}
