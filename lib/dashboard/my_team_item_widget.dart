import 'package:am_flutter/dashboard/DragDropItem.dart';
import 'package:am_flutter/dashboard/dragdrop_item_header_widget.dart';
import 'package:am_flutter/native/test_native_screen.dart';
import 'package:flutter/material.dart';

class MyTeamItemWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return buildCard(context);
  }

  Widget buildCard(BuildContext context) {
    final item = DragDropItem(null, WidgetFeature.MY_TEAM, WidgetType.MENU);
    return Card(
        child: GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TestNativeScreen()),
        );
      },
      child: Container(
          height: 100,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/dashboard_item/stars_elements.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Stack(
            children: [
              DragDropItemHeaderWidget(item),
              Container(
                margin: EdgeInsets.fromLTRB(0, 16, 16, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Image.asset("images/dashboard_item/icon_oneonone.png"),
                  ],
                ),
              ),
            ],
          )),
    ));
  }
}
