import 'package:am_flutter/domain/entity/user.dart';
import 'package:flutter/material.dart';
import 'package:am_flutter/domain/entity/dashboard.dart';

class HeaderWidget extends StatelessWidget {
  final Future<Dashboard> dashboard;
  final Future<User> user;

  HeaderWidget({Key key, @required this.dashboard, @required this.user});

  @override
  Widget build(BuildContext context) {
    final topRow = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 8.0),
              child: Text(
                'P',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            )),
        Container(
          padding: EdgeInsets.only(right: 8.0),
          child: Icon(
            Icons.notifications_active,
            color: Colors.red,
          ),
        )
      ],
    );

    final avatar = Container(
      padding: EdgeInsets.only(top: 72.0, bottom: 8.0),
      child: Container(
        child: CircleAvatar(
          backgroundImage: AssetImage('images/oscar.png'),
          radius: 40.0,
        ),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
            color: Color(0xFF06B5B3),
            width: 4.0,
          ),
        ),
      ),
    );

    return Stack(
      children: <Widget>[
        _setCoverPhoto(),
        Column(
          children: <Widget>[
            topRow,
            avatar,
            _setUsernameText()
          ],
        ),
      ],
    );
  }

  Widget _setCoverPhoto() {
    return FutureBuilder<Dashboard>(
      future: dashboard,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Image.network(
            snapshot.data.bannerUrl,
            width: 640.0,
            height: 150.0,
            fit: BoxFit.cover,
          );
        }
        return Image.asset(
          'images/banner_home.png',
          width: 640.0,
          height: 150.0,
          fit: BoxFit.cover,
        );
      },
    );
  }

  Widget _setUsernameText() {
    return FutureBuilder<User>(
      future: user,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Text(
            snapshot.data.name,
            style: TextStyle(
                color: Color(0xFF0B3051),
                fontWeight: FontWeight.bold,
                fontSize: 32.0),
          );
        } else {
          return Text(
            'Unknown Guy',
            style: TextStyle(
                color: Color(0xFF0B3051),
                fontWeight: FontWeight.bold,
                fontSize: 32.0),
          );
        }
      },
    );
  }
}
