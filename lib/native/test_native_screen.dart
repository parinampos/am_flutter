import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TestNativeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Test Native Screen"),
      ),
      body: Center(
          child: Container(
        color: Colors.amber,
        width: double.infinity,
        height: double.infinity,
      )),
    );
  }
}