import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:am_flutter/domain/entity/dashboard.dart';

const _url = 'https://api.myjson.com/bins/1chtqc';

class DashboardApi {

  Future<Dashboard> fetchDashboard(http.Client client) async {
    final response = await client.get(_url);
    if (response.statusCode == 200) {
      return Dashboard.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load dashboard data');
    }
  }
}