import 'package:am_flutter/domain/entity/dashboard.dart';
import 'package:am_flutter/data/dashboard_api.dart';

import 'package:http/http.dart' as http;

class DashboardInteractor {

  var client = http.Client();

  Future<Dashboard> fetchDashboard() {
    final api = DashboardApi();
    return api.fetchDashboard(client);
  }
}
