import 'package:am_flutter/domain/entity/user.dart';
import 'package:am_flutter/platform_data_provider.dart';

class UserInteractor {
  final _platformDataProvider = PlatformDataProvider();

  Future<User> getUser() async {
    return _platformDataProvider.getPlatformData('getUserData').then((map) {
      final user = User(name: map['name'].toString());
      return user;
    });
  }
}
