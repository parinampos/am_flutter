import 'package:json_annotation/json_annotation.dart';

part 'dashboard.g.dart';

@JsonSerializable()
class Dashboard {
  Dashboard(
      {this.bannerUrl,
      this.highlights,
      this.reward,
      this.learning,
      this.announcement,
      this.messaging,
      this.coaching,
      this.assignment,
      this.survey,
      this.sendStar,
      this.classroomTraining});

  @JsonKey(name: 'bannerUrl')
  final String bannerUrl;

  @JsonKey(name: 'highlight')
  final List<DHighlight> highlights;

  @JsonKey(name: 'reward')
  final DReward reward;

  @JsonKey(name: 'learning')
  final DLearning learning;

  @JsonKey(name: 'announcement')
  final DAnnouncement announcement;

  @JsonKey(name: 'messaging')
  final DMessaging messaging;

  @JsonKey(name: 'coaching')
  final DCoaching coaching;

  @JsonKey(name: 'assignment')
  final DAssignment assignment;

  @JsonKey(name: 'survey')
  final DSurvey survey;

  @JsonKey(name: 'send_star')
  final DSendStar sendStar;

  @JsonKey(name: 'classroom_training')
  final DClassroomTraining classroomTraining;

  factory Dashboard.fromJson(Map<String, dynamic> json) =>
      _$DashboardFromJson(json);
}

@JsonSerializable()
class DHighlight {
  DHighlight(
      {this.feature, this.title, this.imageUrl, this.hasNewNotification});

  @JsonKey(name: 'feature')
  final String feature;
  @JsonKey(name: 'title')
  final String title;
  @JsonKey(name: 'imageUrl')
  final String imageUrl;
  @JsonKey(name: 'hasNewNotification')
  final bool hasNewNotification;

  factory DHighlight.fromJson(Map<String, dynamic> json) =>
      _$DHighlightFromJson(json);
}

@JsonSerializable()
class DReward {
  DReward({this.receivedStar});

  @JsonKey(name: 'receivedStar')
  final int receivedStar;

  factory DReward.fromJson(Map<String, dynamic> json) =>
      _$DRewardFromJson(json);
}

@JsonSerializable()
class DLearning {
  DLearning({this.assigned, this.open, this.inProgress});

  @JsonKey(name: 'assigned')
  final int assigned;
  @JsonKey(name: 'open')
  final int open;
  @JsonKey(name: 'inProgress')
  final int inProgress;

  factory DLearning.fromJson(Map<String, dynamic> json) =>
      _$DLearningFromJson(json);
}

@JsonSerializable()
class DAnnouncement {
  DAnnouncement(this.newAnnouncement);

  @JsonKey(name: 'new')
  final int newAnnouncement;

  factory DAnnouncement.fromJson(Map<String, dynamic> json) =>
      _$DAnnouncementFromJson(json);
}

@JsonSerializable()
class DMessaging {
  DMessaging(this.newMessage);

  @JsonKey(name: 'new')
  final int newMessage;

  factory DMessaging.fromJson(Map<String, dynamic> json) =>
      _$DMessagingFromJson(json);
}

@JsonSerializable()
class DCoaching {
  DCoaching({this.today, this.missed});

  @JsonKey(name: 'today')
  final int today;
  @JsonKey(name: 'missed')
  final int missed;

  factory DCoaching.fromJson(Map<String, dynamic> json) =>
      _$DCoachingFromJson(json);
}

@JsonSerializable()
class DAssignment {
  DAssignment({this.newAssignment});

  @JsonKey(name: 'new')
  final int newAssignment;

  factory DAssignment.fromJson(Map<String, dynamic> json) =>
      _$DAssignmentFromJson(json);
}

@JsonSerializable()
class DSurvey {
  DSurvey({this.newSurvey});

  @JsonKey(name: 'new')
  final int newSurvey;

  factory DSurvey.fromJson(Map<String, dynamic> json) =>
      _$DSurveyFromJson(json);
}

@JsonSerializable()
class DSendStar {
  DSendStar({this.remainingQuota});

  @JsonKey(name: 'remaining_quota')
  final int remainingQuota;

  factory DSendStar.fromJson(Map<String, dynamic> json) =>
      _$DSendStarFromJson(json);
}

@JsonSerializable()
class DClassroomTraining {
  DClassroomTraining({this.today});

  @JsonKey(name: 'today')
  final int today;

  factory DClassroomTraining.fromJson(Map<String, dynamic> json) =>
      _$DClassroomTrainingFromJson(json);
}
