import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
    var navigationController: UINavigationController!
    var flutterViewController: FlutterViewController!

    override func application(_ application: UIApplication,
                              didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        flutterViewController = window?.rootViewController as? FlutterViewController
        navigationController = UINavigationController(rootViewController: flutterViewController)
        navigationController.isNavigationBarHidden = true
        window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        listenToFlutterRouter()
        listenToFlutterDataProvider()

        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    private func listenToFlutterRouter() {
        let channel = FlutterMethodChannel(name: "com.am_flutter/router",
                                           binaryMessenger: flutterViewController)
        channel.setMethodCallHandler { [weak self] (call, result) in
            switch call.method {
            case "gotoAnnouncements" : self?.gotoAnnouncements()
            case "gotoAnnouncementDetail" : do {
                let dict = call.arguments as! NSDictionary
                self?.gotoAnnouncementDetail(id: dict["id"] as! Int)
                }
            default: result(FlutterMethodNotImplemented)
            }
        }
    }
   
    private func gotoAnnouncements() {
        let announcementsViewController = AnnouncementsViewController(nibName: "AnnouncementsViewController", bundle: nil)
        navigationController.pushViewController(announcementsViewController, animated: true)
    }
    
    private func gotoAnnouncementDetail(id: Int) {
        let detailVC = AnnouncementDetailViewController(nibName: "AnnouncementDetailViewController", bundle: nil)
        detailVC.id = id
        navigationController.pushViewController(detailVC, animated: true)
    }
    
    private func listenToFlutterDataProvider() {
        let channel = FlutterMethodChannel(name: "com.am_flutter/data",
                                           binaryMessenger: flutterViewController)
        channel.setMethodCallHandler { [weak self] (call, result) in
            switch call.method {
            case "getUserData" : self?.getUsername(result: result)
            default: result(FlutterMethodNotImplemented)
            }
        }
    }
    
    private func getUsername(result: FlutterResult) {
        result([
            "name" : "Oscar CM BOY"
            ])
    }
}
