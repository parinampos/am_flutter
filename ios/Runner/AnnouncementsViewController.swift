//
//  AnnouncementsViewController.swift
//  Runner
//
//  Created by ampos on 1/9/19.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import Foundation

class AnnouncementsViewController : UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = true
    }
}
