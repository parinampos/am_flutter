//
//  AnnouncementDetailViewController.swift
//  Runner
//
//  Created by ampos on 1/9/19.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import Foundation

class AnnouncementDetailViewController : UIViewController {
    
    var id: Int = 0
    
    @IBOutlet weak var idLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        idLabel.text = "ID: \(id)"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = true
    }
}
