import 'package:am_flutter/data/dashboard_api.dart';
import 'package:test_api/test_api.dart';

import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';

class MockClient extends Mock implements http.Client {}

void main() {
  var dashboardApi = DashboardApi();
  var client = MockClient();

  test('throw exception when fetch dashboard', () {
    when(client.get('https://api.myjson.com/bins/1chtqc'))
        .thenAnswer((_) async => http.Response('Not found', 404));

    expect(dashboardApi.fetchDashboard(client), throwsException);
  });
}
