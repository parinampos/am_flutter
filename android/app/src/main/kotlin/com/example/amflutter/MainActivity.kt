package com.example.amflutter

import android.content.Intent
import android.os.Bundle

import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

private const val ROUTER_CHANNEL = "com.am_flutter/router"
private const val DATA_CHANNEL = "com.am_flutter/data"

class MainActivity : FlutterActivity() {

    private val routerMethodChannel by lazy { MethodChannel(flutterView, ROUTER_CHANNEL) }
    private val dataMethodChannel by lazy { MethodChannel(flutterView, DATA_CHANNEL)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)
        listenToFlutterRouter()
        listenToFlutterDataProvider()
    }

    private fun listenToFlutterRouter() {
        routerMethodChannel.setMethodCallHandler { call, result ->
            when (call.method) {
                "gotoAnnouncements" -> gotoAnnouncementsScreen()
                "gotoAnnouncementDetail" -> {
                    call.argument<Int>("id")?.let {
                        gotoAnnouncementDetailScreen(it)
                    }
                }
                else -> result.notImplemented()
            }
        }
    }

    private fun gotoAnnouncementsScreen() {
        val intent = Intent(this, AnnouncementsActivity::class.java)
        startActivity(intent)
    }

    private fun gotoAnnouncementDetailScreen(id: Int) {
        val intent = Intent(this, AnnouncementDetailActivity::class.java)
        intent.putExtra("id", id)
        startActivity(intent)
    }

    private fun listenToFlutterDataProvider() {
        dataMethodChannel.setMethodCallHandler { call, result ->
            when (call.method) {
                "getUserData" -> result.success(getUsername())
                else -> result.notImplemented()
            }
        }
    }

    private fun getUsername() = mapOf("name" to "Oscar CM BOY")
}
