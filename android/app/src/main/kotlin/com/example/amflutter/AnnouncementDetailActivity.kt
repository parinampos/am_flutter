package com.example.amflutter

import android.app.Activity
import android.os.Bundle
import android.widget.TextView

class AnnouncementDetailActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id = intent.extras["id"] as Int
        val textView = TextView(this)
        textView.text = "Announcement Detail ID: $id"
        setContentView(textView)
    }
}