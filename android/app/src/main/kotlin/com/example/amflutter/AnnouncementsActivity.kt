package com.example.amflutter

import android.app.Activity
import android.os.Bundle
import android.widget.TextView

class AnnouncementsActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val textView = TextView(this)
        textView.text = "Announcement List"
        setContentView(textView)
    }
}